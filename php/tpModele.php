<?php

session_start();
include("config.php");
include("db.php");

function verif_authent() {
	global $AUTHENT;
	if(($AUTHENT == 1) && (!isset($_session["nomuser"]))) {
		header("Location: tpConnexion.php");
	}
}

function config() {
	global $nom_hote, $nom_user, $nom_base, $pwd;/*???*/
	$_SESSION["nomhote"] = $nom_hote;
	$_SESSION["nombase"] = $nom_base;
	$_SESSION["nomuser"] = $nom_user;
	$_SESSION["mdp"] = $pwd;
}

function get_client($numCLi)
{
	if($db = db_connect()) {
		$req = "SELECT nom_client, debit_client FROM client WHERE num_client = " . test_input($numCLi);
		$result = db_query($db, $req);
		if(!result) {
			echo "Erreur dans la requete";
			db_close($db);
			return false;
		}
		else if(pg_num_rows($result) == 0) {
			echo "Pas de resultat";
			db_close($db);
			return false;
		}
		$arr = pg_fetch_array($result);
		echo "<table><tr>
				<td>Nom:".
				$arr[nom_client].
				", </td><td>Debit: ".
				$arr["debit_client"].
				"</td></tr></table>";/*???*/
		db_close($db);
		return true;
	}
	else {
		return false;
	}
}

function get_client_infos($numCli)
{
	if($db = db_connect()) {
		$req = "SELECT nom_client, debit_client FROM client WHERE num_client = ".
			test_input($numCli);
		$result = db_query($db, $req);
		if(!$result) {
			echo "Erreur dans la requete";
			db_close($db);
			return null;
		}
		else if(pg_num_rows($result) == 0) {
			echo "Pas de resultat";
			db_close($db);
			return null;
		}
		$arr = pg_fetch_array($result);
		db_close($db);
		return $arr;
	}
	else {
		return null;
	}
}

function insert_achat($numCli, $montant) {
	if($db = db_connect()) {
		$req = "SELECT debit_client FROM client WHEREnum_client =".test_input($numCli);
		$result = db_query($db, $req);
		$debit = pg_fetch_result($result, "debit_client");
		if($debit < $montant) {
			echo "montant disponible insuffisant";
			db_close($db);
			return false;
		}
		else {
			$soldeMod = $debit - $montant;
			$req = "UPDATE client SET debit_client = \"".format_number($soldeMod)."\" WHERE num_client = ".test_input($numCli);
			$req2 = "INSERT INTO achat(montant_achat, data_achat, client) VALUES (".format_number(test_input($montant)).",'".data("Y-m-d H:i:s")."','".test_input($numCli)."')";/*这标点太可怕*/
			$req_arry = array($req1, $req2);
			db_transaction($db, $req_array);
			db_close($db);
			return true;
		}
	}
	else
		return false;
}

function create_client($numCli, $nomCli, $debitInit) {
	if($db = db_connect()) {
		$req = "INSERT INTO client(num_client, nom_client, debit_client) VALUES (".test_input($numCli).",'".test_input($nomCli)."','".format_number(test_input($debitInit))."')";
		$req = db_query($db, $req);
		if(!$req) {
			affiche_erreur("La creation a echouee.");
		}
		echo "L'utilisation".test_input($nomCli)."(n°".test_input($numCli).") a bien ete cree";
		db_close($db);
		return true;
	}
	else {
		return false;
	}
}

function set_client($numCli, $nommod, $debitMod) {
	if($db = db_connect()) {
		$req = "UPDATE client SET nom_client = \"".test_input($nomMod)."\", debit_client = \"".format_number(test_input($debitMod))."\"WHERE num_client = ".test_input($numCli);
		$result = db_query($db, $req);
		if(!result) {
			db_clse($db);
			return false;
		}
		db_close($db);
		return true;
	}
	else
		return false;
}

function verif_mdp($mdp) {
	global $pwd;
	if($mdp == pwd) {
		config();
		if($db = db_connect()) {
			db_close($db);
			return true;
		}
		else
			return false;
	}
	else
		return false;
}

function detruire_session() {
	session_destroy();
}

function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

function format_number($str) {
	return str_replace(',', '.', $str);
}






















