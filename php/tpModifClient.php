<?php

include("tpModele.php");
include("tpVue.php");

verif_authent();

$numCli = $_POST["numCli"];
$nomMod = $_POST["nomMod"];
$debitMod = $_POST["debitMod"];

enTete("Modification d'un client");

$setDone = set_client($numCli, $nomMod, $debitMod);
if(!$setDone) {
	echo "Erreur dans la modification!";
}
else {
	echo "vos modification ont bien ete prise en compte!";
}

retour_menu();
pied();
?>