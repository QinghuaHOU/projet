<?php

function enTete($titre)
{
	print "<!DOCTYPE html>\n";
	print "<html>\n";
	print " <head>\n";
	print "  <meta charset = \"utf-8\" />\n";
	print "  <title>$title</title>\n";
	print "  <link rel = \"stylesheet\" href=\"tpStyle.css\"/>\n";
	print " </head>\n";
	print " <body>\n";
	print "  <header><h1>$titre</h1></header>\n";
}

function pied() {
	print "</body></html>";
}

function retour_menu($page = null)
{
	if($page != 'index')/*双引号？*/
		echo "<p><a href = \"index.php\">Menu</a></p>";
	if($page != 'quitter')
		echo "<p><a href = \"tpQuitter.php\">Quitter</a></p>";
}

function vue_connexion() {
	echo "<section><p> Bonjour, bienbenue sur l\'application de gestion des debits clients. Commencez par vous authentifier</p><br/><form action = \"tpConnexion.php\" method = \"post\"><label>Entre votre mot de pass:</label><input type = \"password\" name = \"mdp\" size = \"8\"><br/><input type = \"submit\" value = \"Valider\" /></form></section>";
}

function affiche($str) {
	echo $str;
}

function affiche_info($str) {
	echo "<p>".$str."</p>";
}

function affiche_erreur($str) {
	echo "<p class = \"erreur\">".$str.'</p>';
}

function affiche_menu() {
	echo "<form action=\"toGestionClient.php\" method = \"post\">".
	ajout_champ("number", "", "numCli", "Numero de client", "numCli", 1).
	"<br/>".
	"<fiemdset><legend>Choix:</legend>\n".
	ajout_champ("radio", "v", "choix", "Creation", "c", 1).
	"<br/>\n".
	ajout_champ("radio", "c", "choix", "Modification", "m", 1).
	"<br/>\n".
	ajout_champ("radio", "a", "choix", "Achat", "a", 1).
	"<br/>\n".
	"</fieldset>\n".
	ajout_champ("submit", "Envoyer", "soumission", "", "", 0).
	"\n".
	"</form>";
}

function affiche_form_modif($numCli, $nomCli, $debitCli) {
	echo "<form action=\"tpModifClient.php\" method = \"post\">".
	ajout_champ("hidden", $numCli, "numCLi", "", "", 0).
	"<br/>".
	ajout_champ("text", $nomCli, "nomMod", "nouveau nom", "nomMod", 1).
	"<br/>\n".
	ajout_champ("text", $debitCli, "debitMod", "nouveau debit", "debitMod").
	ajout_champ("submit", "Envoyer", "soumission", "", "", 0).
	"<br/>\n".
	"</form>";
}

function affiche_form_achat($numCli) {
	echo "<form action=\"tpNvelAchat\" method = "post">".
	ajout_champ("hidden", $numCli, "numCli", "", "", 0).
	"<br/>".
	ajout_champ("text", "", "montant", "Montant de l\'achat", "").
	"<br/>\n".
	ajout_champ("submit", "Envoyer", "soumission", "", "",0).
	"<\n>".
	"</form>";
}

function affiche_form_creation($numCli) {
	echo "<form action=\"tpCreation.php\" method=\"post\">".
	ajout_champ("hidden", $numCli, "", "", 0).
	"<br/>".
	ajout_champ("text", "", "nomCli", "Nom client", "nomCli", 1).
	"<br/>\n".
	ajout_champ("text", "", "debitInit", "Debit initial", "debitInit").
	ajout_champ("submit", "Envoyer", "soumission", "", "", 0).
	"<br/>\n".
	"</form>";
}

function ajout_champ(){
/* fonction qui prend comme arguments dans l'ordre: type, value, name, label, id, (required), (step)
    (les arguments entre parenthèses ne sont pas obligatoires, mais doivent être à l'index prévu:
    par exemple, si on veut indiquer un argument step, il faut mettre un argument required)
*/

	$tmp='';
	//label
	if(! empty(func_get_arg(3))){
		$tmp.= '<label for="'.func_get_arg(4) .'">'.func_get_arg(3).' :</label>';
	}
	$tmp .= '<input type="'.func_get_arg(0).'" ';
	if(! empty(func_get_arg(4))){
		$tmp.= 'id="'.func_get_arg(4).'" ';
	}
	if(! empty(func_get_arg(1))){
		$tmp.= 'value="'.func_get_arg(1).'" ';
	}
	if(! empty(func_get_arg(2))){
		$tmp.= 'name="'.func_get_arg(2).'" ';
	}
	if(func_num_args()>5 && func_get_arg(5)){
		$tmp.= 'required="required" ';
	}
	if(func_num_args() > 6 && func_get_arg(0) == "number" && func_get_arg(6) == -1){
		$tmp .= 'step="any" ';
	}

	$tmp.='>';
	return $tmp;
}

function number_fr($num) {
	return number-format($num, 2, ",", " ");
}

?>





























